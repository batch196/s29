db.products.insertMany([
{
"name": "IphoneX",
"price": 30000,
"isActive": true
},
{
"name": "Samsung Galaxy S21",
"price": 51000,
"isActive": true
},
{
"name": "Razor Blackhawk v2X",
"price": 2000,
"isActive": false
},
{
"name": "Razer Mechanical Keyboard",
"price": 4000,
"isActive": true
}
])

// db.products.insertMany([
// {
// "name": "IphoneX",
// "price": 30000,
// "isActive": true
// },
// {
// "name": "Samsung Galaxy S21",
// "price": 51000,
// "isActive": true
// },
// {
// "name": "Razor Blackhawk v2X",
// "price": 2000,
// "isActive": false
// },
// {
// "name": "Razer Mechanical Keyboard",
// "price": 4000,
// "isActive": true
// }
// ])

db.products.find({price:{$gt:3000}})

db.products.find({price:{$lt: 3000}})

db.products.find({price:{$gte: 30000}})


{
    "_id" : ObjectId("62e0e0175db01896603075c7"),
    "firstname" : "Mary Jane",
    "lastname" : "Watson",
    "email" : "mjtiger@gmail.com",
    "password" : "tigerjackpot15",
    "isAdmin" : false
}

/* 2 */
{
    "_id" : ObjectId("62e0e0175db01896603075c8"),
    "firstname" : "Gwen",
    "lastname" : "Stacy",
    "email" : "stacytech@gmail.com",
    "password" : "stacytech1991",
    "isAdmin" : true
}

/* 3 */
{
    "_id" : ObjectId("62e0e0175db01896603075c9"),
    "firstname" : "Peter",
    "lastname" : "Parker",
    "email" : "peterwebdev@gmail.com",
    "password" : "peterwebdev",
    "isAdmin" : false
}

/* 4 */
{
    "_id" : ObjectId("62e0e0175db01896603075ca"),
    "firstname" : "Jonah",
    "lastname" : "Jameson",
    "email" : "jjjameson@gmail.com",
    "password" : "spideyisamenace",
    "isAdmin" : false
}

/* 5 */
{
    "_id" : ObjectId("62e0e0175db01896603075cb"),
    "firstname" : "Otto",
    "lastname" : "Octavius",
    "email" : "ottoOctopi@gmail.com",
    "password" : "docOck15",
    "isAdmin" : false
}

db.users.find({"firstname": {$regex: 'O'}})
//looks for documents with partial match and by default case sensitive

db.users.find({"firstname": {$regex: 'o', $options: '$i'}})
//not case sensitive

db.products.find({"name":{$regex: 'phone', $options: '$i'}})


db.users.find({"email":{$regex: 'web', $options: '$i'}})

db.products.find({"name":{$regex: 'razer', $options: '$i'}})

//multiple finds
db.products.find({$or:[{name: {$regex: 'x',$options: '$i'}},{price:{$lte: 10000}}]})

db.products.find({$or:[{name: {$regex: 'x',$options: '$i'}},{price:{$gte: 30000}}]})


db.products.find({$and:[{name: {$regex: 'razer',$options: '$i'}},{price:{$gte: 3000}}]})

db.products.find({$and:[{name: {$regex: 'x',$options: '$i'}},{price:{$gte: 30000}}]})

db.users.find({$and:[{lastname: {$regex: 'w',$options: '$i'}},{isAdmin: false}]})
db.users.find({$and:[{firstname: {$regex: 'a',$options: '$i'}},{isAdmin: true}]})






db.users.find({isAdmin:true},{_id:0,email:1})
db.users.find({isAdmin:false},{firstname:1,lastname:1})

db.product.find({price:{$gte:10000}},{_id:0,name:1,price:1})