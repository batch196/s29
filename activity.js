db.courses.insertMany(
    [

    {
        "name": "HTML Basics",
        "price": 20000,
        "isActive": true,
        "instructor": "Sir Alvin"        
    },
        {
        "name": "CSS 101 + Flexbox",
        "price": 21000,
        "isActive": true,
        "instructor": "Sir Alvin"
        
        },
        {
        "name": "Javascript 101",
        "price": 32000,
        "isActive": true,
        "instructor": "Ma'am Tine"
        
        },
        {
        "name": "Git 101, IDE and CLI",
        "price": 19000,
        "isActive": false,
        "instructor": "Ma'am Tine"
        
        },
        {
        "name": "React JS",
        "price": 25000,
        "isActive": true,
        "instructor": "Ma'am Miah"
        
        }
    ]
 )
db.courses.find(
        {
            $and:[
                {instructor: {$regex: 'sir alvin',$options: '$i'}},
                {price:{$gte: 20000}},
            ]
        },
        {_id:0,name:1, price:1}
    )
 db.courses.find(
       {
           $and:[
                {instructor: {$regex: 'Ma\'am Tine', $options: '$i'}},
                {isActive: false}           
           ]
       },
       {price:1,name:1,_id:0}
   )
db.courses.find(
       {
           $and:[
                 {name: {$regex: 'r', $options: '$i'}},
                 {price: {$lte: 25000}}
           ]
       }
   ) 
       
//update all courses with price less than 21000 to inacttive
 db.courses.updateMany(
       
                {price: {$lt: 21000}}, 
                
              {$set:
                {isActive: false}
              }
   )

 db.courses.deleteMany({ price: { $gte: 25000 } })